const {
    PORT,
    API_KEY: apiKey = 'sampleapikey123',
    DB_PATH: dbPath = './db/data-back.json'
} = process.env

const port = Number(PORT) | 3000

module.exports = {
    port,
    apiKey,
    dbPath
}