/**
 * @typedef {object} DataRecord
 * @property {string} id
 * @property {string} name
 * @property {number} lat
 * @property {number} lng
 * @property {string} address
 * @property {string} status
 */

/**
 * @typedef {object} DataSource
 * @property {(filter?: object) => Promise<T[]>} readAll
 * @property {(id: string, data: T) => Promise<T>} updateById
 * @template T
 */


/**
 * @typedef {object} AppService
 * @property {(status?: string) => Promise<DataRecord[]>} list
 * @property {() => DataRecord|null} readRandomNotDelivered
 */


const STATUS_DELIVERED = 'delivered'

/**
 * 
 * @param {DataSource<DataRecord>} ds 
 * @returns {AppService}
 */
const service = (ds) => {
    return {
        list: async (status) => {
            if (!status) {
                return ds.readAll()
            }
            return ds.readAll({ status })
        },
        readRandomNotDelivered: async () => {
            const result = await ds.readAll().then(r => r.filter(d => d.status !== STATUS_DELIVERED))
            const randIndex = Math.floor(Math.random() * result.length)
            const record = result[randIndex]
            return ds.updateById(record.id, { status: STATUS_DELIVERED })
        }
    }
}

module.exports = service