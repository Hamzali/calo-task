const router = require('./router')
const config = require('./config')
const service = require('./service')
const datasource = require('./json.datasource')
const event = require('./event')

const fs = require('fs')
const util = require('util')

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const stat = util.promisify(fs.stat)
const COUNTER_FILE_PATH = './total.json'

event.onStatusChange(async () => {
    const data = await stat(COUNTER_FILE_PATH)
        .then(s => {
            if (!s.isFile) {
                throw new Error('invalid file path')
            }
            return readFile(COUNTER_FILE_PATH).then(d => JSON.parse(d.toString()))
        })
        .catch(console.error)

    const { total } = data || { total: 0 }
    writeFile(COUNTER_FILE_PATH, JSON.stringify({ total: total + 1 }))
})

const main = async () => {
    // initialize and inject dependencies
    const appDatasource = await datasource()
    const appService = service(appDatasource)
    const appRouter = router.create()

    // controller layer
    const { GET } = router.HTTP_METHODS
    const { NOT_AUTHORIZED } = router.HTTP_STATUSES

    const API_KEY_HEADER = 'X-Api-Key'

    appRouter.route(GET, '/', async (req) => {
        if (req.headers[API_KEY_HEADER] !== config.apiKey) {
            throw new router.AppError(NOT_AUTHORIZED, 'invalid credentials!')
        }
        return appService.readRandomNotDelivered().then(event.emitStatusChanged)
    })

    appRouter.route(GET, '/list', async (req) => {
        const { status } = router.getQueryParams(req)
        return appService.list(status)
    })

    appRouter.listen(config.port)
}

main()
    .then(() => console.log(`app is up and running on http://localhost:${config.port}`))
    .catch(console.error)