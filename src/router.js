const http = require('http')
const querystring = require('querystring')

class AppError extends Error {
    constructor(status, message) {
        super()
        this.status = status
        this.message = message
    }
}

/**
 * @typedef {string} HttpMethod
 */

/**
 * 
 * @enum {HttpMethod}
 */
const HTTP_METHODS = {
    GET: 'GET'
}

/**
 * @typedef {number} HttpStatus
 */

/**
 * @enum {HttpStatus}
 */
const HTTP_STATUSES = {
    OK: 200,
    NOT_FOUND: 404,
    NOT_AUTHORIZED: 401,
    INTERNAL_SERVER_ERROR: 500
}

/**
 * removes trailing slashes
 * @param {string} url 
 * @returns {string}
 */
const formatUrl = (url) => {
    if (url === '/' || url === '') {
        return '/'
    }
    const pathUrl = url.split('?')[0]
    if (pathUrl[pathUrl.length - 1] === '/') {
        return pathUrl.slice(0, pathUrl.length - 1)
    }
    return pathUrl
}

/**
 * 
 * @param {http.IncomingMessage} req 
 */
const getQueryParams = (req) => {
    return querystring.parse(req.url.split('?')[1])
}

const create = () => {
    /**
     * @type {Record<string, Record<string, http.RequestListener>>}
     */
    const handlers = http.METHODS.reduce((acc, method) => {
        acc[method] = {}
        return acc
    }, {})

    const server = http.createServer(async (req, res) => {
        const pathUrl = formatUrl(req.url)
        const handler = handlers[req.method] && handlers[req.method][pathUrl]
        if (!handler) {
            res.writeHead(HTTP_STATUSES.NOT_FOUND)
            res.end()
            return
        }

        res.setHeader('Content-Type', 'application/json')

        try {
            const result = await handler(req, res)
            if (result) {
                res.write(JSON.stringify(result))
            }
        } catch (err) {
            console.error(err)
            let status = HTTP_STATUSES.INTERNAL_SERVER_ERROR, message = null
            if (err.constructor === AppError.constructor) {
                status = err.status
                message = err.message
            }
            res.writeHead(status)
            if (message) {
                res.write(JSON.stringify({ message }))
            }
        }

        res.end()
    })

    return {
        /**
         * @param {HttpMethod} method
         * @param {string} path
         * @param {http.RequestListener} handler 
         */
        route: (method, path, handler) => {
            if (!handlers[method]) {
                throw new Error('invalid http method for handler!')
            }
            handlers[method][path] = handler
        },
        /**
         * 
         * @param {number} port 
         * @returns 
         */
        listen: (port) => server.listen(port)
    }
}

module.exports = {
    HTTP_METHODS,
    HTTP_STATUSES,
    create,
    getQueryParams,
    AppError
}