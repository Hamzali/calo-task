const fs = require('fs')
const util = require('util')
const config = require('./config')

const fsStat = util.promisify(fs.stat)
const fsRead = util.promisify(fs.readFile)
const fsWrite = util.promisify(fs.writeFile)

const deepMerge = (target, source) => {
    for (const key of Object.keys(source)) {
        if (source[key] instanceof Object) Object.assign(source[key], deepMerge(target[key], source[key]))
    }
    Object.assign(target || {}, source)
    return target
}

module.exports = async () => {
    const stat = await fsStat(config.dbPath)
    if (!stat.isFile) {
        throw new Error(`invalid db path provided; '${config.dbPath}' is not a file!`)
    }

    const writeData = (data) => fsWrite(config.dbPath, JSON.stringify(data))

    const readAll = async (filter) => {
        const data = await fsRead(config.dbPath).then(d => JSON.parse(d.toString()))
        if (filter) {
            return data.filter(d => Object.entries(filter).every(([field, value]) => d[field] === value))
        }
        return data
    }

    const updateById = async (id, updateData) => {
        const data = await readAll()

        const itemIndex = data.findIndex((item) => item.id === id)

        if (itemIndex === -1) {
            throw new Error('id not found in the database!')
        }

        const { id: _idVal, ...rest } = updateData
        const newItem = deepMerge(data[itemIndex], rest)

        await writeData([
            ...data.slice(0, itemIndex),
            newItem,
            ...data.slice(itemIndex + 1)
        ])

        return newItem
    }

    return {
        readAll,
        updateById
    }
}