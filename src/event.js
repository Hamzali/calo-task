const EventEmitter = require('events')

class DataEmitter extends EventEmitter { }
const STATUS_CHANGED = 'status_changed'
const dataEmitter = new DataEmitter()

const emitStatusChanged = () => dataEmitter.emit(STATUS_CHANGED)
const onStatusChange = (cb) => dataEmitter.on(STATUS_CHANGED, cb)

module.exports = {
    emitStatusChanged,
    onStatusChange
}