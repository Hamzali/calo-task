# Calo Task

## How to run?
It is just a barebones NodeJS app so just run `npm start`

following env variables can be used;
- `PORT`: application port to listen
- `API_KEY`: password value to compare for the protected endpoints
- `DB_PATH`: path to the json database file
default values can be found in `./src/config.js`